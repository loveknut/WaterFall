__author__ = 'suphawking'
import sqlite3
import os
from flask import Flask,jsonify,render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('main.html')
    #return '<h1>hello</h1>'
@app.route('/getlastid/', methods=['GET'])
def lastid():
    con = sqlite3.connect('reader.db')
    con.text_factory=str
    cur = con.cursor()
    cur.execute('SELECT id FROM reader ORDER BY id')
    tasks={}
    tasks['date']=len(cur.fetchall())
    cur.close()
    con.close()
    return jsonify({'tasks': [tasks]})
    #return  jsonify({'result',[result]})
@app.route('/getpaper/<int:id>', methods=['GET'])
def getid(id):
    con = sqlite3.connect('reader.db')
    con.text_factory=str
    cur = con.cursor()
    cur.execute("SELECT * FROM reader WHERE id>=(?) and id<=(?) ",(id-9,id))
    data=cur.fetchall()
    cur.close()
    con.close()
    tasks=[]
    for n in data:
        tmp={}
        tmp['id']    =n[0]
        tmp['title'] =n[1]
        tmp['link']  =n[2]
        tasks.append(tmp)
    return jsonify({'tasks': [tasks]})
    #return  jsonify({'result',[result]})
if __name__ == '__main__':
    app.run()

